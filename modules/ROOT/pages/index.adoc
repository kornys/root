= Testing Farm
:keywords: Testing System as a Service, Testing Farm, overview

[discrete.tagline]
== Testing System as a Service

Testing Farm is a reliable and scalable Testing System as a Service for Red Hat internal services, https://cloud.redhat.com[Red Hat Hybrid Cloud services], and open source projects related to Red Hat products. It is commonly used as a test execution back-end of other services or CI systems. Thanks to its https://api.dev.testing-farm.io[HTTP API] it can be easily integrated into any other service.

The tests are abstracted away from the test infrastructure using open-source https://tmt.readthedocs.io/en/latest/[test metadata format] which unifies how Red Hat engineers, upstream developers, contributors, and communities are able to discover, debug and run tests.

Thanks to the test infrastructure abstraction, the tests can ask for specific hardware requirements for their execution, without worrying about which infrastructure they should use. This abstraction also provides transparent provisioning for users of various infrastructure providers.

== Current users

* RHEL CI
* https://docs.fedoraproject.org/en-US/ci[Fedora CI]
* https://packit.dev[Packit]
* https://fedora.softwarefactory-project.io/project/Fedora-Zuul-CI[Zuul CI for Fedora]
* https://centos.softwarefactory-project.io/zuul/t/centos[Zuul CI for CentOS Stream]
* https://gitlab.com/redhat/automotive/automotive-sig[Automotive Toolchain Pipeline of The CentOS Automotive SIG]
* https://github.com/marketplace/actions/schedule-tests-on-testing-farm[Testing Farm Github Action]
