= Contact
:keywords: Contact, Email, Slack

== IM

If you are a Red Hat employee, find us on #testing-farm Slack channel.

If you are from the community, you can find us on https://matrix.to/#/#fedora-ci:fedoraproject.org[Fedora Matrix - Fedora CI channel].
Please mention @mvadkert in the space.

== Email

If you want to contact us via email, use `tft AT redhat.com`
