= Services
:keywords: Services

[#artifacts]
== Artifacts Storage

Testing Farm stores all test artifacts in its Artifacts Storage service. Currently there are two deployments of the artifacts storage.

=== Public Ranch

* Available via https://artifacts.dev.testing-farm.io
* Retention time: 90 days

=== Red Hat Ranch

* Available via https://artifacts.osci.redhat.com/testing-farm
* Retention time: unlimited

[NOTE]
====
Directory index is not provided here, append `/<REQUEST_ID>` to the URL to get the results for a specific request, e.g. http://artifacts.osci.redhat.com/testing-farm/34125b2c-22d6-4ac5-b603-6f7b83046da2/
====

[#cloud-costs]
== Cloud Costs

Testing Farm provides an easy way how to access reports for cloud costs per Testing Farm user.

[IMPORTANT]
====
This feature is available only for Red Hat employees using 🎩 Red Hat ranch.
For Public ranch, the feature is currently not planned.

The cloud costs are being reported only for AWS.
====

The cloud costs reporting is available via AWS web console.

=== Logging into AWS

[IMPORTANT]
====
Logging in is required in order to access the cloud cost dashboard.
====

All employees working in PnT have read-only access to the AWS Cost Explorer.
To log in, use the https://url.corp.redhat.com/it-aws-login[following link].

In case you already have access to AWS, you need to choose the following role:

```
Account: arr-cloud-aws-core (727920394381)
Role: 727920394381-cloud-costs
```

And click `Sign in` button.

=== Accessing Testing Farm Costs Dashboard

To access the dashboard, use the https://url.corp.redhat.com/testing-farm-cloud-costs-last-7-days[following link].

[NOTE]
====
BaseOS CI costs are not included in the report.
If you are interested in BaseOS CI report, see the https://url.corp.redhat.com/baseos-ci-cloud-costs-last-7-days[following link].
====

By default, the dashboard is showing the cloud spend for the last 7 days.

image::testing-farm-costs.png[]

To change the reporting date range, use the `Report Parameters -> Time -> Date Range` on the right side.

To see the cloud costs per team, use the `Cost and usage breakdown` on the bottom.

=== Changing the Team Name

In case you are using Packit service, it is advised to explicitly define the team name for the reporting.
By default, the costs will be reported against Packit service, which interacts with Testing Farm.

See the https://packit.dev/docs/configuration/examples/#tests[Packit configuration example - Tag cloud resources in Testing Farm] for details.
